info4help
=========

Utilitaire listant les informations du système Debian GNU/Linux afin de les poster sur un forum pour obtenir de l'aide.

info4help formate les informations en bbcode ou markdown au choix.

construction, test & install :
------------------------------

    $ equivs-build info4help.equivs
    $ lintian info4help-$VERSION.deb
    # dpkg -i info4help-$VERSION.deb
